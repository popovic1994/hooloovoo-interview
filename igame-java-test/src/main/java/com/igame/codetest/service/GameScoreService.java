package com.igame.codetest.service;

import com.igame.codetest.pojo.UserScore;

public interface GameScoreService {

    /**
     *
     * @return true if list is empty
     */
    boolean isEmpty();

    /**
     *
     * @param userScore
     */
    void insert(UserScore userScore);

    /**
     *
     * @param position
     * @return username at provided position
     */
    String getUserNameAtPosition(int position);

    /**
     *
     * @param position
     * @return word at provided position
     */
    String getWordEntryAtPosition(int position);

    /**
     *
     * @param position
     * @return score at provided position
     */
    Integer getScoreAtPosition(int position);

    void printList();
}
