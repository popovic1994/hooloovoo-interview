package com.igame.codetest.service.impl;

import com.igame.codetest.pojo.UserScore;
import com.igame.codetest.service.AnagramWordMakingGame;
import com.igame.codetest.service.GameScoreService;
import com.igame.codetest.service.WordDictionaryHandler;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AnagramGameImpl implements AnagramWordMakingGame {

    private String aReallyLongWord;
    private WordDictionaryHandler dictionary;
    private GameScoreService gameScoreService;
    private Map<Character, Long> mapOfAllowedLetters;

    public AnagramGameImpl(String aReallyLongWord, WordDictionaryHandler dictionary) {
        this.aReallyLongWord = aReallyLongWord;
        this.dictionary = dictionary;
        this.gameScoreService = new GameScoreServiceImpl();
        this.mapOfAllowedLetters = aReallyLongWord
                .chars()
                .mapToObj(c -> (char) c).collect(Collectors.toList())
                .stream()
                .collect(Collectors
                        .groupingBy(Function.identity(), Collectors.counting()));
    }

    @Override
    public int submitWord(String username, String word) {
        int score = 0;
        if (allowedLettersUsed(word)) {
            if (dictionary.contains(word)) {
                score = word.length();
                gameScoreService.insert(new UserScore(username, score, word));
                gameScoreService.printList();
            }
        }

        return score;
    }

    private boolean allowedLettersUsed(String word) {
        if (containsWhitespace(word) || containsSpecialCharsOrUppercase(word)) return false;
        Map<Character, Long> mapOfSubmittedLetters = word
                .chars()
                .mapToObj(c -> (char) c).collect(Collectors.toList())
                .stream()
                .collect(Collectors
                        .groupingBy(Function.identity(), Collectors.counting()));

        for (Map.Entry<Character, Long> entry : mapOfSubmittedLetters.entrySet()) {
            Character letter = entry.getKey();
            if (!mapOfAllowedLetters.containsKey(letter) ||
                    mapOfAllowedLetters.get(letter) < mapOfSubmittedLetters.get(letter)
            ) {
                return false;
            }
        }

        return true;
    }

    private boolean containsSpecialCharsOrUppercase(String word) {
        return !word.matches("\\p{javaLowerCase}*");
    }

    private boolean containsWhitespace(String word) {
        return word.contains(" ");
    }

    @Override
    public String getUserNameAtPosition(int position) {
        return gameScoreService.getUserNameAtPosition(position);
    }

    @Override
    public String getWordEntryAtPosition(int position) {
        return gameScoreService.getWordEntryAtPosition(position);
    }

    @Override
    public Integer getScoreAtPosition(int position) {
        return gameScoreService.getScoreAtPosition(position);
    }
}
