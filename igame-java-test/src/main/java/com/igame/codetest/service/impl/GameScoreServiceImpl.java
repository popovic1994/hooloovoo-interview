package com.igame.codetest.service.impl;

import com.igame.codetest.config.PropertiesConfig;
import com.igame.codetest.pojo.UserScore;
import com.igame.codetest.service.GameScoreService;

public class GameScoreServiceImpl implements GameScoreService {
    private Node head;
    private Node tail;
    private int size;
    private static final int maxNumberOfScores = Integer
            .parseInt(PropertiesConfig.getInstance().getProperty("maxNumberOfScores"));

    private class Node {
        private UserScore userScore;
        private Node next;
        private Node previous;

        Node(UserScore userScore, Node next, Node previous) {
            this.userScore = userScore;
            this.next = next;
            this.previous = previous;
        }

        Node(UserScore userScore) {
            this(userScore, null, null);
        }
    }

    public GameScoreServiceImpl() {
        head = null;
        tail = null;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public synchronized void insert(UserScore userScore) {
        Node node = new Node(userScore);

        if (isEmpty()) {
            head = node;
            tail = node;
            incrementSize();
        } else if (head.userScore.getScore() < node.userScore.getScore()) {
            addToFirstPosition(node);
            incrementSize();
        } else {
            Node temp = head;
            int position = 1;
            while (temp.userScore.getScore() >= node.userScore.getScore()) {
                if (position == maxNumberOfScores) return;
                if (temp.userScore.getUsername() == node.userScore.getUsername()
                        && temp.userScore.getWord() == node.userScore.getWord()) return;

                if (temp.next == null) {
                    addToLastPosition(temp, node);
                    incrementSize();
                    return;
                }
                temp = temp.next;
                position++;
            }
            addBetweenFirstAndLastPosition(temp, node);
            incrementSize();
        }

        if (size > maxNumberOfScores) {
            removeFromLastPosition();
            decrementSize();
        }
    }

    @Override
    public String getUserNameAtPosition(int position) {
        if (position == size-1) return tail.userScore.getUsername();
        if (position == 0) return head.userScore.getUsername();

        Node node = findNodeAtPosition(position);
        return node != null ? node.userScore.getUsername() : null;
    }

    @Override
    public String getWordEntryAtPosition(int position) {
        if (position == size-1) return tail.userScore.getWord();
        if (position == 0) return head.userScore.getWord();

        Node node = findNodeAtPosition(position);
        return node != null ? node.userScore.getWord() : null;
    }

    @Override
    public Integer getScoreAtPosition(int position) {
        if(position == size-1) return tail.userScore.getScore();
        if(position == 0) return head.userScore.getScore();

        Node node = findNodeAtPosition(position);
        return node != null ? node.userScore.getScore() : null;
    }

    private Node findNodeAtPosition(int position) {
        if (isEmpty() || position > size-1 || position < 0) return null;

        Node tmpNode;
        if (position <= (maxNumberOfScores-1)/2) {
            tmpNode = head;
            int currPosition = 0;

            while (currPosition != position) {
                tmpNode = tmpNode.next;
                currPosition++;
            }
        } else {
            tmpNode = tail;
            int currPosition = size-1;

            while (currPosition != position) {
                tmpNode = tmpNode.previous;
                currPosition--;
            }
        }

        return tmpNode;
    }

    private void incrementSize() {
        size++;
    }

    private void decrementSize() {
        size--;
    }

    private void addToFirstPosition(Node node) {
        head.previous = node;
        node.next = head;
        head = node;
    }

    private void addToLastPosition(Node currLast, Node node) {
        node.previous = currLast;
        currLast.next = node;
        tail = node;
    }

    private void addBetweenFirstAndLastPosition(Node nextNode, Node node) {
        node.next = nextNode;
        node.previous = nextNode.previous;
        nextNode.previous = node;
        node.previous.next = node;
    }

    private void removeFromLastPosition() {
        tail = tail.previous;
        tail.next = null;
    }

    @Override
    public void printList() {
        Node tempNode = head;

        System.out.println("Current score >>>");
        while (tempNode != null) {
            System.out.print("UserScore{" +
                    "username='" + tempNode.userScore.getUsername() + '\'' +
                    ", score=" + tempNode.userScore.getScore() +
                    ", word='" + tempNode.userScore.getWord() + '\'' +
                    '}' + '\n'
            );
            tempNode = tempNode.next;
        }
    }
}
