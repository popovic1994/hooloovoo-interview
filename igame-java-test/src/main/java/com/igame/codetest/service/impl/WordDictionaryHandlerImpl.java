package com.igame.codetest.service.impl;

import com.igame.codetest.service.WordDictionaryHandler;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WordDictionaryHandlerImpl implements WordDictionaryHandler {

    private static WordDictionaryHandler wordDictionaryHandler;
    private List<String> words = new ArrayList<>();

    public synchronized static WordDictionaryHandler getInstance() {
        if (wordDictionaryHandler == null) {
            wordDictionaryHandler = new WordDictionaryHandlerImpl();
        }

        return wordDictionaryHandler;
    }

    private WordDictionaryHandlerImpl() {
        HttpClient httpClient = HttpClient
                .newBuilder()
                .followRedirects(HttpClient.Redirect.ALWAYS)
                .build();

        try {
            HttpResponse<String> response = httpClient.send(HttpRequest.newBuilder()
                    .uri(URI.create("http://www.scrabble.org.nz/assets/CSW15.txt"))
                    .GET()
                    .build(), HttpResponse.BodyHandlers.ofString());

            words = new ArrayList<>(Arrays.asList(response.body().split("\\r\n")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean contains(String word) {
        return words.contains(word.toUpperCase());
    }

    @Override
    public int size() {
        return words.size();
    }
}
