package com.igame.codetest.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesConfig {
    private final Properties configProp = new Properties();
    private static PropertiesConfig propertiesConfig;

    private PropertiesConfig() {
        InputStream in = this.getClass()
                .getClassLoader()
                .getResourceAsStream("application.properties");
        try {
            configProp.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized static PropertiesConfig getInstance() {
        if (propertiesConfig == null) {
            propertiesConfig = new PropertiesConfig();
        }
        return propertiesConfig;
    }

    public String getProperty(String key){
        return configProp.getProperty(key);
    }

    public boolean containsKey(String key){
        return configProp.containsKey(key);
    }
}
