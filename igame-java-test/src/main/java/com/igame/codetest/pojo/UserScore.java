package com.igame.codetest.pojo;

public class UserScore {
    private String username;
    private Integer score;
    private String word;

    public UserScore(String username, Integer score, String word) {
        this.username = username;
        this.score = score;
        this.word = word;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
