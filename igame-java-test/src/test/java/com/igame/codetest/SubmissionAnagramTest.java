package com.igame.codetest;

import com.igame.codetest.service.AnagramWordMakingGame;
import com.igame.codetest.service.WordDictionaryHandler;
import com.igame.codetest.service.impl.AnagramGameImpl;
import com.igame.codetest.service.impl.WordDictionaryHandlerImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SubmissionAnagramTest {
    WordDictionaryHandler dictionary;
    AnagramWordMakingGame service;

    public SubmissionAnagramTest() {
        dictionary = WordDictionaryHandlerImpl.getInstance();
    }

    @Before
    public void setUp() throws Exception {
        service = new AnagramGameImpl("areallylongword", dictionary);
    }

    @Test
    public void testSubmission() throws Exception {
        assertEquals(2, service.submitWord("1", "no"));
        assertEquals(4, service.submitWord("2", "grow"));
        assertEquals(0, service.submitWord("3", "bold"));
        assertEquals(0, service.submitWord("4", "glly"));
        assertEquals(6, service.submitWord("5", "woolly"));
        assertEquals(0, service.submitWord("6", "adder"));
        assertEquals(6, service.submitWord("7", "really"));

        //additional tests
        assertEquals(0, service.submitWord("8", "aNagraM"));
        assertEquals(0, service.submitWord("9", "ana gram"));
        assertEquals(0, service.submitWord("10", "a*na-gram"));
    }
}