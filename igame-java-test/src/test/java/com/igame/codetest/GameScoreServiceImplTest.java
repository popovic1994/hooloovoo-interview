package com.igame.codetest;

import com.igame.codetest.pojo.UserScore;
import com.igame.codetest.service.GameScoreService;
import com.igame.codetest.service.impl.GameScoreServiceImpl;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class GameScoreServiceImplTest extends TestCase {

    GameScoreService service;

    @Before
    public void setUp() {
        service = new GameScoreServiceImpl();
    }

    @Test
    public void testScoreListOrder() {
        service.insert(new UserScore("Ana", 3, "dog"));
        service.insert(new UserScore("Aco", 5, "house"));
        service.insert(new UserScore("Milos", 9, "important"));
        service.insert(new UserScore("Ivan", 2, "no"));
        service.insert(new UserScore("Marija", 4, "life"));

        assertEquals("Milos", service.getUserNameAtPosition(0));
        assertEquals(Integer.valueOf(9), service.getScoreAtPosition(0));
        assertEquals("important", service.getWordEntryAtPosition(0));

        assertEquals("Marija", service.getUserNameAtPosition(2));
        assertEquals(Integer.valueOf(4), service.getScoreAtPosition(2));
        assertEquals("life", service.getWordEntryAtPosition(2));

        assertEquals("Ivan", service.getUserNameAtPosition(4));
        assertEquals(Integer.valueOf(2), service.getScoreAtPosition(4));
        assertEquals("no", service.getWordEntryAtPosition(4));

        //now we add 5 more
        service.insert(new UserScore("Ana", 6, "ignore"));
        service.insert(new UserScore("Aco", 8, "elephant"));
        service.insert(new UserScore("Milos", 12, "successfully"));
        service.insert(new UserScore("Ivan", 3, "job"));
        service.insert(new UserScore("Marija", 8, "mountain"));
        service.insert(new UserScore("Marija", 9, "developer"));

        assertEquals("Milos", service.getUserNameAtPosition(0));
        assertEquals(Integer.valueOf(12), service.getScoreAtPosition(0));
        assertEquals("successfully", service.getWordEntryAtPosition(0));

        assertEquals("Ivan", service.getUserNameAtPosition(9));
        assertEquals(Integer.valueOf(3), service.getScoreAtPosition(9));
        assertEquals("job", service.getWordEntryAtPosition(9));

        assertNull(service.getUserNameAtPosition(10));
        assertNull(service.getScoreAtPosition(10));
        assertNull(service.getWordEntryAtPosition(10));

        assertNull(service.getUserNameAtPosition(-1));
        assertNull(service.getScoreAtPosition(-1));
        assertNull(service.getWordEntryAtPosition(-1));
    }
}