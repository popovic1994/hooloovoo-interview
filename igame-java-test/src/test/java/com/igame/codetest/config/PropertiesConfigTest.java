package com.igame.codetest.config;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PropertiesConfigTest {
    PropertiesConfig propertiesConfig;

    @Before
    public void setUp() {
        propertiesConfig = PropertiesConfig.getInstance();
    }

    @Test
    public void testPropertyExists() {
        assertTrue(propertiesConfig.containsKey("maxNumberOfScores"));
    }

    @Test
    public void testPropertyValue() {
        assertEquals("10", propertiesConfig.getProperty("maxNumberOfScores"));
    }
}